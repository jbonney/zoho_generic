#!/usr/bin/ruby

# Import users from a MySQL DB to Zoho CRM
# @author: Jimmy Bonney (jbonney@d-sight.com)
# @created_at: 2012-05-14
# @sponsor: D-Sight (http://www.d-sight.com)
# @version: 1.0

# Dependencies
require 'rubygems'
require 'active_record'
require 'httparty'
require 'csv'
require 'mail'

# [UPDATE WITH YOUR VALUES]
# Email setup
Mail.defaults do
  delivery_method :smtp, { 
    :address              => 'mail.yourcompany.com',
    :port                 => 25,
    :domain               => "yourcompany.com",
    :user_name            => 'user',
    :password             => 'abcdefgh',
    :authentication       => 'plain',
    :enable_starttls_auto => false }
end

# [UPDATE WITH YOUR VALUES]
# Set up active record connection
ActiveRecord::Base.establish_connection(
  :adapter => "mysql2",
  :encoding => "utf8", 
  :database => "database_name",
  :username => "database_user",
  :password => "abcdefgh",
  :host => "localhost",
  :timeout => 5000
)

# [UPDATE WITH YOUR MODELs AS DEFINED IN YOUR APPLICATION]
# Client Accounts
class Account < ActiveRecord::Base
end

# Users
class User < ActiveRecord::Base  
end

# Roles
class Role < ActiveRecord::Base
end

# User Roles
class UserRoleMapping < ActiveRecord::Base
end

# Define class to connect to Zoho CRM API
class ZohoCRM
  include HTTParty
end

# Script directory
path = File.expand_path(File.dirname(__FILE__))
# Get the historical data
previous_executions = CSV.read(path + "/history.csv", :headers => :true)
# Get the last execution data
last_execution = previous_executions[previous_executions.size-1]
# Get the last execution date (this is the first column)
last_date = DateTime.parse last_execution['Last Execution Time']
puts "Last execution date: "
puts last_date

# Set the new execution date (now)
new_date = DateTime.now
# Set the date format used in file name
date_info = new_date.strftime("%Y%m%d")

# Get the accounts created during the period [last_date, new_date]
accounts = Account.where("created_at > ? and created_at < ?", last_date, new_date - 1.seconds).all
puts "Number of accounts created since last time: "
puts accounts.count

# Save how many accounts are correctly sent to Zoho
successfully_saved = 0
# Prepare hash to save any errors than would occur
errors = Hash.new

# [UPDATE WITH YOUR MODEL STRUCTURE]
# Go through the accounts list
accounts.each do |account|
  owner = Role.where("name = ? and authorizable_id = ?", 'owner', account.id).first
  user_role_id = UserRoleMapping.where("role_id = ?", owner.id).first.user_id
  user = User.where("id = ?", user_role_id).first
  puts "User email: "
  puts user.email

  # Prepare data to be sent to Zoho
  xml_data = "<Leads><row no=\"1\"><FL val=\"Lead Source\">Your Web Application</FL><FL val=\"First Name\">#{user.first_name}</FL><FL val=\"Last Name\">#{user.last_name}</FL><FL val=\"Email\">#{user.email}</FL></row></Leads>"
 
  # [UPDATE WITH YOUR Authentication Token]
  puts "Sending user data to Zoho"
  response = ZohoCRM.post('https://crm.zoho.com/crm/private/xml/Leads/insertRecords', :body => 
    {:newFormat => '1', :authtoken => '[YOUR_AUTH_TOKEN]', :scope => 'crmapi', :xmlData => xml_data})

  # Register success or failure
  if response.code < 300
    puts "User added successfully to Zoho"
    successfully_saved += 1
  else
    puts "An error occured while adding user"
    errors[account.id] = response.parsed_response["response"]["error"]
    # Add entry that would not have been sent to Zoho in a CSV file
    CSV.open(path + "/" + date_info + "_manual_import.csv", "a") do |csv|
      csv << ['Date', 'Account ID', 'Email', 'First Name', 'Last Name'] if errors.size == 1
      csv << [new_date, account.id, user.email, user.first_name, user.last_name]
    end
  end
end

# Save history
CSV.open(path + "/history.csv", "a") do |csv|
  csv << [new_date, accounts.count, successfully_saved]
end

puts "History updated"

# [UPDATE WITH YOUR VALUES]
# Send email in case of failure
if successfully_saved != accounts.count
  puts "Sending email to inform administrator that manual import to Zoho is required"
  mail = Mail.deliver do
    to 'info@yourcompany.com'
    from 'Web Application <info@yourcompany.com>'
    subject '[Action Required] Zoho Import Error'
    text_part do
      body "An error occured while importing contacts to Zoho. Please take a look at the attached CSV file and import manually in Zoho."
    end
    html_part do
      content_type 'text/html; charset=UTF-8'
      body "An error occured while importing contacts to Zoho. Please take a look at the attached CSV file and import manually in Zoho."
    end
    attachments[date_info + "_manual_import.csv"] = {  
      :mime_type => 'text/csv',
      :content => File.read(path + "/" + date_info + "_manual_import.csv") 
    }
  end
end

puts "Imported #{successfully_saved}/#{accounts.count} accounts to Zoho successfully" if accounts.count > 0
